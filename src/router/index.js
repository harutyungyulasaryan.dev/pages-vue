import Vue from 'vue';
import Router from 'vue-router';
import Layout from '../components/Layout';
import FormElements from '../components/FormElements';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Layout',
      component: Layout,
        children: [
            {
                path: '/form_elements',
                name: 'FormElements',
                component: FormElements,
            }
        ],
    }
  ]
})
