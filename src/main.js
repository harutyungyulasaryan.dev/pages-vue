// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import vSelect from 'vue-select';
import VueLodash from 'vue-lodash';
import App from './App';
import router from './router';

import './assets/plugins/pace/pace-theme-flash.css';
import './assets/plugins/boostrapv3/css/bootstrap.min.css';
import './assets/plugins/font-awesome/css/font-awesome.css';
import './assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.min.css';
import './assets/plugins/summernote/css/summernote.css';
import './pages/css/pages-icons.css';
import './pages/css/pages.css';

Vue.use(VueLodash, {});

Vue.component('v-select', vSelect);

Vue.config.productionTip = false;
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
});
